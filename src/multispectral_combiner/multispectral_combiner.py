import numpy as np
from scipy.misc import imread, imsave


def __get_coefficients(wave_length):
    """
    Converting wave length to channel weights
    # https://stackoverflow.com/questions/3407942/rgb-values-of-visible-spectrum

    Parameters
    ----------
    wave_length : float
        Wave length

    Returns
    -------
    r : float
        Weight of the red channel
    g: float
        Weight of the green channel
    b: float
        Weight of the blue channel
    """
    coefficients = None
    if 380 <= wave_length < 440:
        r = -(wave_length - 440.) / (440. - 380.)
        g = 0.0
        b = 1.0
    elif 440 <= wave_length < 490:
        r = 0.0
        g = (wave_length - 440.) / (490. - 440.)
        b = 1.0
    elif 490 <= wave_length < 510:
        r = 0.0
        g = 1.0
        b = -(wave_length - 510.) / (510. - 490.)
    elif 510 <= wave_length < 580:
        r = (wave_length - 510.) / (580. - 510.)
        g = 1.0
        b = 0.0
    elif 580 <= wave_length < 645:
        r = 1.0
        g = -(wave_length - 645.) / (645. - 580.)
        b = 0.0
    elif 645 <= wave_length <= 780:
        r = 1.0
        g = 0.0
        b = 0.0
    else:
        r = 0.0
        g = 0.0
        b = 0.0
    return r, g, b


def multispectral_to_rgb(files, output_file):
    """
    Merging multispectral images to RGB image

    Parameters
    ----------
    files : list
        List of multispectral images file names. The list should be sorted in order of increasing wavelengths
    output_file : string
        Output file name
    """
    start_wave_length = 400
    end_wave_length = 700
    max_grey_scale_value = 65536
    result_image = None
    wave_length = start_wave_length
    step = (end_wave_length - start_wave_length) / len(files)
    total_weights = np.array([0., 0., 0.])

    for file in files:
        img = imread(file)
        if result_image is None:
            result_image = np.zeros((img.shape[0], img.shape[1], 3))
        # Scaling 16 bit greyscale image to 0-1 range
        img = img / max_grey_scale_value
        # Obtaining RGB coefficients
        r, g, b = __get_coefficients(wave_length)
        total_weights += np.array([r, g, b])
        # We increase the value of pixels taking into account the channel weights
        result_image[:, :, 0] += r * img
        result_image[:, :, 1] += g * img
        result_image[:, :, 2] += b * img
        wave_length += step
    # We scale the image by dividing by the weights of channel
    result_image = result_image / total_weights
    imsave(output_file, result_image)
