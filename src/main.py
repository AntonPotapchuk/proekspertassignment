import os

from src.multispectral_combiner.multispectral_combiner import multispectral_to_rgb

if __name__ == "__main__":
    input_dir = "../multispectral_images/"
    output_dir = "./output/output.png"
    files = [input_dir + file for file in os.listdir(input_dir)]
    multispectral_to_rgb(files, output_dir)