# Proekspert test assignment

---

## Idea

The algorithm is very simple.

I use [weighted average](https://en.wikipedia.org/wiki/Weighted_arithmetic_mean) for obtaining the original image. I use R, G, B coefficients as a weights for each channel.


## Dependencies

List of dependencies:

- [NumPy](http://www.numpy.org/)
- [SciPy](https://www.scipy.org/)

__Install from pip__


```
pip install numpy scipy
```

## How to use

You can use provided code in the next way:


```
multispectral_to_rgb(files, output_dir)
```

Here "files" is a list of pathes to multispectral images. The list should be sorted in order of increasing wavelengths, "output_dir" is a path to the output file.

__Example:__
```
import os

from src.multispectral_combiner.multispectral_combiner import multispectral_to_rgb

if __name__ == "__main__":
    input_dir = "../multispectral_images/"
    output_dir = "./output/output.png"
    files = [input_dir + file for file in os.listdir(input_dir)]
    multispectral_to_rgb(files, output_dir)
```


## Results

You can find the result of the algorithm in the output folder.